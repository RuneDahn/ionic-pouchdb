import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TesteDbProvider } from '../../providers/teste-db/teste-db';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    lista: ItemModel[] = [];

    constructor(public navCtrl: NavController, private testeDb: TesteDbProvider) { }

    async ionViewDidLoad  () {
        this.testeDb.createDb();
        this.carregarItems();
    }
    
    async carregarItems() {        
        await this.testeDb.selectAll();
        this.lista = this.testeDb.items;
    }

    async inserir (nome) {
        await this.testeDb.insert(nome);
        this.carregarItems();
    }

    async editar (item: ItemModel, novoNome: string) {
        item.Nome = novoNome;
        await this.testeDb.update(item);
        this.carregarItems();
    }

    async deletar (item: ItemModel) {
        await this.testeDb.delete(item);
        this.carregarItems();
    }
}