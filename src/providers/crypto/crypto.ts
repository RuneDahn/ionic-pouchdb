import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CryptoProvider {
    public static createUuidv4() {
        let getRandom = () => crypto.getRandomValues(new Uint8Array(1))[0];
        let generateRandom = (c: any) => (c ^ getRandom() & 15 >> c / 4).toString(16)
        let template = 1e7.toString()+-1e3+-4e3+-8e3+-1e11;
        return template.replace(/[018]/g, generateRandom);            
    }
}
