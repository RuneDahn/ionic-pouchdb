import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import cordovaSqlitePlugin from 'pouchdb-adapter-cordova-sqlite';
import { CryptoProvider } from '../crypto/crypto';

@Injectable()
export class TesteDbProvider {
    private db;
    public items;

    createDb() {
        PouchDB.plugin(cordovaSqlitePlugin);
        this.db = new PouchDB('items.db', {
            adapter: 'cordova-sqlite'
        });
    }

    async insert (nome: string) {
        var item: ItemModel = {
            _id: CryptoProvider.createUuidv4(),
            _rev: null,
            Nome: nome,
            Data_Criacao: new Date()
        };

        return await this.db.post(item);
    }

    async update (item: ItemModel) {
        return await this.db.put(item);
    }

    async delete (item: ItemModel) {
        return await this.db.remove(item);
    }

    async selectById (id: string) {
        return await this.db.get(id);
    }

    async selectAll (): Promise<ItemModel[]> {
        let option = { include_docs: true };
        let docsLoadFinished = docs => this.items = docs.rows.map(item => item.doc);

        let allItems = () =>
            this.db.allDocs(option)
                .then(docsLoadFinished);

        this.db.changes({
            live: true,
            since: 'now',
            include_docs: true
        }).on('change', () => {
            allItems().then(items => this.items = items)
        });

        return await allItems();
    }
}
